# Great Hacker War

The Great Hacker War was a purported 1990–1992 (perhaps 1997) conflict between the Masters of Deception (MOD) and an unsanctioned splinter faction of the older guard hacker group Legion of Doom (LOD), amongst several smaller subsidiary groups. Both of the primary groups involved made attempts to hack into the opposing group's networks, across the Internet, X.25, and telephone networks. In a panel debate of The Next HOPE conference, 2010, Phiber Optik re-iterated that the rumoured "gang war in cyberspace" between LOD and MOD never happened, and that it was "a complete fabrication" by the U.S attorney's office and some sensationalist media. [[The Next HOPE (2010): Informants: Villains or Heroes?]] Furthermore, two other high-ranking members of the LOD confirmed that the "Great Hacker War" never occurred, reinforcing the idea that this was just a competition of one-upsmanship and not a war.

[The Next HOPE (2010): Informants: Villains or Heroes?]: https://www.youtube.com/watch?v=Jnk5HRIocu8&t=40m5s

